import os, sys

def header():                           #PyJuice Header
    clear_screen()
    print("---------- PyJuice ----------\n")

def clear_screen():						#Clear Screen function - OS Specific
	if sys.platform == 'linux2' or sys.platform == 'linux':
		os.system('clear')
	elif sys.platform == 'darwin':
		os.system('clear')
	elif sys.platform == 'win32':
		os.system('cls')
	else:
		print("\n") * 5

def int_check(a):						#Check user input is an integer
	check = a.isdigit()
	if check == True:
		return int(a)
	else:
		return False

def float_check(a):                     #Check user input is a float
  try:
    float(a)
    return True
  except ValueError:
    return False

def percentage(percent, whole):         #Calculate Percentage
  return (percent * whole) / 100.0

def nic_weight(strenght, material):     #Calculate Nicotine weight
    w_nic = strenght / 10
    juice_quant = 100 - w_nic
    juice_weight = juice_quant * material
    w_nic = w_nic * 1.01
    w_total = juice_weight + w_nic
    w_total = w_total / 100
    return w_total

weight = ["PG", 1.038, "VG", 1.26]      #Weight list

header()
amount = int(input("How many ML of juice do you want to make? > "))

vg_ratio = int(input("What percent of VG do you want to use? > "))
pg_ratio = 100 - vg_ratio
symbol = "%"
print("You selected %d%sVG & %d%sPG" % (vg_ratio, symbol, pg_ratio, symbol))

s_nic = int(input("What is your target nicotine strenght (mg/ml)? > "))
b_nic = int(input("What is your base nicotine strenght (mg/ml)? > "))
m_nic = input("What is your nicotine base material (pg or vg)? > ")

if m_nic == "pg" or m_nic == "PG":
    w_nic = nic_weight(b_nic, 1.038)
elif m_nic == "vg" or m_nic == "VG":
    w_nic = nic_weight(b_nic, 1.26)
else:
    quit("That's not a valid material shit lord...")

flav_num = int(input("How many flavours do you want to use? > "))
if flav_num > 0:
    q_flav = range(0, flav_num)
    f_lst1 = []
    for f in q_flav:
        fl = []
        fl.append(input("What is the flavor? > "))
        fl.append(int(input
            ("What strenght do you want your flavour (percent)? > ")))
        fl.append(input
            ("What is the base material of your flvour (pg or vg)? > "))
        if fl[2] != "vg" and fl[2] != "VG" and fl[2] != "pg" and fl[2] != "PG":
            quit("That's not a valid material shit lord...")
        f_lst1.append(fl)
else:
    f_lst1 = [["No Flavors", 0, "pg"]]

output = {}
output["Total"] = amount
output["VG"] = percentage(vg_ratio, amount)
output["PG"] = percentage(pg_ratio, amount)
output["Nicotine"] = (amount * s_nic) / b_nic
output["Flavors"] = f_lst1
if m_nic == "pg" or m_nic == "PG":
    output["PG"] = output["PG"] - output["Nicotine"]
elif m_nic == "vg" or m_nic == "VG":
    output["VG"] = output["VG"] - output["Nicotine"]
else:
    quit("Nic_output")
for f in output["Flavors"]:
    if f[2] == "pg" or f[2] == "PG":
        output["PG"] = output["PG"] - percentage(f[1], amount)
    elif f[2] == "vg" or f[2] == "VG":
        output["VG"] = output["VG"] - percentage(f[1], amount)
    else:
        quit(falvor_output)

header()
print(str(output["Total"]) + "ml of Juice")
print(str(round(output["VG"], 2)) + "ml of VG" + " - "
    + str(round(output["VG"] * weight[3], 2)) + "mg")
print(str(round(output["PG"], 2)) + "ml of PG" + " - "
    + str(round(output["PG"] * weight[1], 2)) + "mg")
print(str(round(output["Nicotine"], 2)) + "ml of Nicotine" + " - "
    + str(round(output["Nicotine"] * w_nic, 2)) + "mg")
for f in output["Flavors"]:
    if f[2] == "pg" or "PG":
        print(str(round(percentage(f[1], amount), 2)) + "ml of " + f[0] + " - "
            + str(round(percentage(f[1], amount) * weight[1], 2)) + "mg")
    else:
        print(str(round(percentage(f[1], amount), 2)) + "ml of " + f[0] + " - "
            + str(round(percentage(f[1], amount) * weight[3], 2)) + "mg")
print(input("\nPress return to contine..."))
clear_screen()
