PyJuice

A simple e-juice calculator, including weight, for Python 3.

This doesn't currently support pg/vg mixes of nicotine or flavor, but I will add this feature.